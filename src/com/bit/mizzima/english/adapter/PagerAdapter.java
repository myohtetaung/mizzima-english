package com.bit.mizzima.english.adapter;

import com.bit.mizzima.english.R;
import com.bit.mizzima.english.fragment.About;
import com.bit.mizzima.english.fragment.News;
import com.bit.mizzima.english.fragment.Setting;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {

	Context c;

	public PagerAdapter(Context c, FragmentManager fm) {
		super(fm);
		this.c = c;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		Fragment f = null;
		if (position == 0) {
			f = new News();
		} else if (position == 1) {
			f = new About();
		} else if (position == 2) {
			f = new Setting(c);
		} else {
			f = new News();
		}

		return f;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "News";
		case 1:
			return "About";
		case 2:
			return "Setting";
		}
		return null;
	}

	public int getIcon(int position) {
		switch (position) {
		case 0:
			return R.drawable.btn_new;
		case 1:
			return R.drawable.btn_about;
		case 2:
			return R.drawable.btn_setting;
		}
		return 0;
	}

	public int getSelectedIcon(int position) {
		switch (position) {
		case 0:
			return R.drawable.btn_new_active;
		case 1:
			return R.drawable.btn_about_active;
		case 2:
			return R.drawable.btn_setting_active;
		}
		return 0;
	}

}
