package com.bit.mizzima.english.adapter;

import java.util.ArrayList;

import com.bit.mizzima.english.DetailPager;
import com.bit.mizzima.english.R;
import com.bit.mizzima.english.object.News;
import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.util.ViewHolder;


import androd.httpimage.FileSystemPersistence;
import androd.httpimage.HttpImageManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class NewsAdapter extends ArrayAdapter<News> {

	SharedPreferences pref;
	// Typeface tf;
	ViewHolder vh;
	HttpImageManager iManager;

	public NewsAdapter(Context context, int textViewResourceId,
			ArrayList<News> news) {
		super(context, textViewResourceId, news);
		// TODO Auto-generated constructor stub
		pref = getContext().getSharedPreferences(Constants.SHARED_PRED,
				Activity.MODE_PRIVATE);
		vh = new ViewHolder();
		// tf = Util.mm(getContext());
		iManager = new HttpImageManager(new FileSystemPersistence(
				Constants.BASEDIR));
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflate = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate.inflate(R.layout.newslist_item, null);
		} else {
			LayoutInflater inflate = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflate.inflate(R.layout.newslist_item, null);
			convertView = v;
		}

		vh.photo = (ImageView) convertView.findViewById(R.id.img_thumb);
		vh.title = (TextView) convertView.findViewById(R.id.txt_title);
		vh.facebook = (ImageView) convertView.findViewById(R.id.btn_facebook);
		vh.email = (ImageView) convertView.findViewById(R.id.btn_email);

		// vh.title.setTypeface(tf);
		vh.title.setText(getItem(position).title);
		vh.title.setTextSize(pref.getInt("FONTSIZE", Constants.smallest));
		if (getItem(position).thumblink != null
				&& getItem(position).thumblink.trim() != "") {
			vh.photo.setTag(getItem(position).thumblink);
			Uri uri = Uri.parse(getItem(position).thumblink.trim()
					.replace("https://", "http://").replace("%3A", ":")
					.replace("%2F", "/"));
			if (uri != null && getItem(position).thumblink.trim() != ""
					&& getItem(position).thumblink.trim() != null) {
				System.out.println(uri);
				Bitmap bitmap = iManager
						.loadImage(new HttpImageManager.LoadRequest(uri,
								vh.photo));
				if (bitmap != null) {
					vh.photo.setImageBitmap(bitmap);
				}
			}
		}

		if (getItem(position).is_read == 0) {
			vh.title.setTextColor(Color.BLACK);
		} else if (getItem(position).is_read == 1) {
			vh.title.setTextColor(Color.GRAY);
		}

		vh.facebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, getItem(position).alternate);
				getContext().startActivity(
						Intent.createChooser(intent, "Share with"));
			}
		});

		vh.email.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
				i.putExtra(Intent.EXTRA_TEXT, getItem(position).alternate);
				try {
					getContext().startActivity(
							Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(getContext(),
							"There are no email clients installed.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getContext(), DetailPager.class);
				i.putExtra("NPOS", position);
				getContext().startActivity(i);
			}
		});

		return convertView;
	}
}
