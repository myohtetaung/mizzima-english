package com.bit.mizzima.english.adapter;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockActivity;

import com.bit.mizzima.english.R;
import com.bit.mizzima.english.object.Adv;
import com.bit.mizzima.english.object.Image;
import com.bit.mizzima.english.object.News;
import com.bit.mizzima.english.util.*;

import androd.httpimage.HttpImageManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;

import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailPagerAdapter extends PagerAdapter {

	ArrayList<News> news;

	DBManager dbm;
	HttpImageManager mManager;
	SherlockActivity c;
	// Typeface tf;
	int imgpos = 0;
	SharedPreferences pref;

	public DetailPagerAdapter(SherlockActivity context, DBManager dbm,
			HttpImageManager mManager, ArrayList<News> news) {
		// TODO Auto-generated constructor stub
		c = context;
		this.news = news;
		this.dbm = dbm;
		this.mManager = mManager;
		pref = c.getSharedPreferences(Constants.SHARED_PRED, c.MODE_PRIVATE);
		// tf = Util.mm(c.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return news.size();
	}

	@Override
	public Object instantiateItem(View collection, int position) {
		News n = news.get(position);
		final ArrayList<Image> images = dbm.getAllFBImages(n.id);
		View v = getDetailNewsView(position, n, images);

		((ViewPager) collection).addView(v, 0);

		return v;
	}

	private View getDetailNewsView(int position, final News n,
			final ArrayList<Image> images) {
		LayoutInflater lf = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = lf.inflate(R.layout.news_detail, null);

		ViewHolder holder = new ViewHolder();

		TextView title = (TextView) v.findViewById(R.id.txt_title);
		holder.photo = (ImageView) v.findViewById(R.id.img_detail);
		TextView detail = (TextView) v.findViewById(R.id.txt_content);
		holder.facebook = (ImageView) v.findViewById(R.id.btn_facebook);
		holder.email = (ImageView) v.findViewById(R.id.btn_email);
		holder.adv = (ImageView) v.findViewById(R.id.img_adv);

		// title.setTypeface(tf);
		// detail.setTypeface(tf);

		title.setText(Html.fromHtml(n.title));
		detail.setText(Html.fromHtml(n.content));

		title.setTextSize(pref.getInt("FONTSIZE", Constants.smallest));
		detail.setTextSize(pref.getInt("FONTSIZE", Constants.smallest));

		holder.email.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
				i.putExtra(Intent.EXTRA_TEXT, n.alternate);
				try {
					c.startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(c, "There are no email clients installed.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		holder.facebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, "http://goo.gl/VEsS5");
				c.startActivity(Intent.createChooser(intent, "Share with"));
			}
		});

		if (n.thumblink != null && n.thumblink.trim() != "") {
			holder.photo.setTag(n.thumblink.trim()
					.replace("https://", "http://").replace("%3A", ":")
					.replace("%2F", "/"));
			Uri uri = Uri.parse(n.thumblink.trim()
					.replace("https://", "http://").replace("%3A", ":")
					.replace("%2F", "/"));
			if (uri != null && n.thumblink.trim() != ""
					&& n.thumblink.trim() != null) {
				Bitmap bitmap = mManager
						.loadImage(new HttpImageManager.LoadRequest(uri,
								holder.photo));
				if (bitmap != null) {
					holder.photo.setImageBitmap(bitmap);
				}
			}
		}

		if (!images.isEmpty()) {

			holder.photo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Dialog d = new Dialog(c);
					d.requestWindowFeature(Window.FEATURE_NO_TITLE);
					d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
					d.setContentView(R.layout.image_dialog);
					d.setCanceledOnTouchOutside(true);

					ImageView photo = (ImageView) d.findViewById(R.id.image);
					photo.setVisibility(ImageView.GONE);
					final ViewPager pager = (ViewPager) d
							.findViewById(R.id.pager);
					TextView txtLabel = (TextView) d.findViewById(R.id.text);
					// txtLabel.setTypeface(tf);

					PagerAdapter adpimg = new PagerAdapter() {

						@Override
						public int getCount() {
							return images.size();
						}

						@Override
						public Object instantiateItem(View collection,
								int position) {
							Image n = images.get(position);
							View v = createImageView(n, position + 1,
									images.size());

							((ViewPager) collection).addView(v, 0);

							return v;
						}

						@Override
						public void destroyItem(View collection, int position,
								Object view) {
							((ViewPager) collection).removeView((View) view);
						}

						@Override
						public boolean isViewFromObject(View view, Object object) {
							return view == ((View) object);
						}

						@Override
						public void finishUpdate(View arg0) {
						}

						@Override
						public void restoreState(Parcelable arg0,
								ClassLoader arg1) {
						}

						@Override
						public Parcelable saveState() {
							return null;
						}

						@Override
						public void startUpdate(View arg0) {
						}
					};
					pager.setAdapter(adpimg);
					pager.setCurrentItem(imgpos);
					pager.setOnPageChangeListener(new OnPageChangeListener() {

						@Override
						public void onPageSelected(int arg0) {
							// TODO Auto-generated method stub
							imgpos = arg0;
						}

						@Override
						public void onPageScrolled(int arg0, float arg1,
								int arg2) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onPageScrollStateChanged(int arg0) {
							// TODO Auto-generated method stub

						}
					});

					d.show();
				}
			});
		}

		final Adv adv = dbm.getAdv(2);
		catchLog(adv.photo);

		String udid = android.provider.Settings.System.getString(
				c.getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		final String link = pref.getString("tracker_link",
				Constants.URL_TRACKER)
				+ "?adid="
				+ adv.adid
				+ "&url="
				+ adv.url
				+ "&app_id=BITMZM0009"
				+ "&udid="
				+ udid
				+ "&platform=2";

		if (!adv.photo.trim().equalsIgnoreCase("") && adv.photo != null) {
			holder.adv.setTag(adv.photo);
			Uri uri = Uri.parse(adv.photo);
			if (uri != null) {
				Bitmap bitmap = mManager
						.loadImage(new HttpImageManager.LoadRequest(uri,
								holder.adv));
				if (bitmap != null) {
					holder.adv.setImageBitmap(bitmap);
				}
			}
		}

		holder.adv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent a = new Intent(Intent.ACTION_VIEW);
				catchLog(link);
				a.setData(Uri.parse(link));
				c.startActivity(a);
			}
		});

		return v;
	}

	private View createImageView(Image n, int pos, int full) {

		LayoutInflater lf = (LayoutInflater) c.getLayoutInflater();
		View v = lf.inflate(R.layout.image_dialog_zoomable, null);

		ViewHolder holder = new ViewHolder();
		holder.touchphoto = (TouchImageView) v.findViewById(R.id.image);
		holder.detail = (TextView) v.findViewById(R.id.indicator);

		holder.touchphoto.setMaxZoom(4f);

		Uri uri = null;

		if (n.image_link.contains("ak.fbcdn.net")) {
			uri = Uri.parse(n.image_link
					.substring(0, n.image_link.length() - 5).replace(
							"https://", "http://")
					+ "n.jpg");
		} else {
			uri = Uri.parse(n.image_link.replace("https://", "http://")
					.replace("%3A", ":").replace("%2F", "/"));
		}

		if (uri != null) {
			Bitmap bitmap = mManager
					.loadImage(new HttpImageManager.LoadRequest(uri,
							holder.touchphoto));
			if (bitmap != null) {
				holder.touchphoto.setImageBitmap(bitmap);
			}
		}

		holder.detail.setText(pos + " of " + full);

		return v;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View) view);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((View) object);
	}

	@Override
	public void finishUpdate(View arg0) {
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View arg0) {
	}

	private void catchLog(String log) {
		Log.i("GetMovies", log);
	}
}
