package com.bit.mizzima.english.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import org.json.JSONObject;

import android.util.Log;

public class HTTPPost {

	public static final String ERROR_CODE_TIME_OUT = "408";
	public static final String ERROR_CODE_UNKNOW_HOST = "404";

	public String sendJSON(JSONObject jo,String url){
		catchLog("postUrlData ======================>>> " + url);
		String result = null;

		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postMethod = new HttpPost(url);
			postMethod.setHeader( "Content-Type", "application/json" );
			postMethod.setEntity(new ByteArrayEntity(jo.toString().getBytes("UTF8")));
			HttpResponse response = httpClient.execute(postMethod);
			catchLog("HTTP Entity : " + EntityUtils.toString(postMethod.getEntity()));
			catchLog("HTTP Response : " + response.getStatusLine());
			catchLog("HTTP Response Code: " + response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				response.getEntity().writeTo(os);
				result = os.toString();
			}

			Header[] headers = response.getAllHeaders();
			for (int i=0; i < headers.length; i++) {
				Header h = headers[i];
				catchLog(h.getName() + ":" + h.getValue());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = "";
			e.printStackTrace();
		}

		return result;
	}

	public String requestFeed(String url){
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpClient.execute(httppost);
			HttpEntity entity = response.getEntity();
			catchLog("HTTP Content Encoding : " + entity.getContentEncoding());
			catchLog("HTTP Content Type : " + entity.getContentType());
			BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent(), "utf-8"),8);
			catchLog("HTTP Response Buffer Reader: " + br.toString());
			StringBuilder sb = new StringBuilder();
			sb.append(br.readLine() + "\n");
			String line = "0";

			while((line = br.readLine()) != null){
				sb.append(line+"\n");
			}
			catchLog("HTTP Response : " + sb.toString());
			catchLog("HTTP Response Code: " + response.getStatusLine().getStatusCode());

			return sb.toString();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "";
		}
	}

	public String getHttpData(String url) {
		catchLog("getUrlData ======================>>> " + url);
		String result = null;
		int timeOutMS = 1000*100;
		HttpResponse response = null;
		try {
			HttpParams my_httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(my_httpParams, timeOutMS);
			HttpConnectionParams.setSoTimeout(my_httpParams, timeOutMS);
			// HttpClient httpClient = new DefaultHttpClient(my_httpParams); //

			DefaultHttpClient client = new DefaultHttpClient(my_httpParams);
			URI uri = new URI(url);
			HttpGet httpGetRequest = new HttpGet(uri);
			//httpGetRequest.setHeader( "Content-Type", "application/json" );
			response = client.execute(httpGetRequest);
			catchLog("HTTP Response : " + response.getStatusLine());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				response.getEntity().writeTo(os);
				result = os.toString();
			}

		}
		catch (Exception e){
			e.printStackTrace();
			result = response.getStatusLine().getStatusCode()+"";
		}

		return result;
	}

	public String toJSONString(String result){
		if(result.startsWith("("))
			return result.substring(1);
		if(!result.startsWith("{")){
			int index = result.indexOf("{");
			return result.substring(index);
		}

		else
			return result;
	}

	private void catchLog(String log){
		Log.i(getClass().getName(), log);
	}
}
