package com.bit.mizzima.english.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.actionbarsherlock.app.SherlockActivity;
import com.bit.mizzima.english.*;
import com.bit.mizzima.english.object.Adv;
import com.bit.mizzima.english.object.Image;
import com.bit.mizzima.english.object.News;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBManager extends SQLiteOpenHelper{
	private SQLiteDatabase db;
	private static Context myContext;
	public static final String DB_NAME = "mizzima.db";
	public String DB_PATH = "/data/data/com.bit.mizzima.english/databases/";

	SharedPreferences pref;
	int storagetime = 1;

	public DBManager(Context context) {
		super(context, DB_NAME, null, 1);
		DBManager.myContext = context;
		try {
			boolean dbexist = checkdatabase();
			if(dbexist)
			{
				//System.out.println("Database exists");
				opendatabase(); 
			}
			else
			{
				System.out.println("Database doesn't exist");
				createdatabase();
				System.out.println(" Database Copied");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		//pref = context.getSharedPreferences(Constants.SHARED_PRED, SherlockActivity.MODE_PRIVATE);
		//storagetime = pref.getInt("STORAGE", 1);
	}

	public void createdatabase() throws IOException{
		boolean dbexist = checkdatabase();
		if(dbexist)
		{
			System.out.println(" Database exists.");
		}
		else{
			this.getReadableDatabase();
			try{
				System.out.println(" Database Copying");
				copydatabase();
			}
			catch(IOException e){
				//throw new Error("Error copying database");
				e.printStackTrace();
			}
		}
	}
	private boolean checkdatabase() {
		//SQLiteDatabase checkdb = null;
		boolean checkdb = false;
		try{
			String myPath = DB_PATH + DB_NAME;
			File dbfile = new File(myPath);
			//checkdb = SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READWRITE);
			checkdb = dbfile.exists();
		}
		catch(SQLiteException e){
			e.printStackTrace();
			System.out.println("Database doesn't exist");
		}

		return checkdb;
	}
	private void copydatabase() throws IOException {

		//Open your local db as the input stream
		InputStream myinput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outfilename = DB_PATH + DB_NAME;
		System.out.println(outfilename);
		//Open the empty db as the output stream
		OutputStream myoutput = new FileOutputStream(outfilename);

		// transfer byte to inputfile to outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myinput.read(buffer))>0)
		{
			myoutput.write(buffer,0,length);
		}

		//Close the streams
		myoutput.flush();
		myoutput.close();
		myinput.close();

	}

	public void opendatabase() throws SQLException
	{
		//Open the database
		String mypath = DB_PATH + DB_NAME;
		db = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);

	}


	private boolean CheckIfExist(String table,String field, String value){
		String query = "SELECT * FROM " + table + " WHERE " + field + " = '" + value + "'";
		//String query = "SELECT * FROM tbl_exchange_rate WHERE uniq_idx = 'RAT018'";
		catchLog("CheckQuery ==> " + query);
		Cursor c = this.db.rawQuery(query, null);
		if (c.moveToFirst()){
			do{
				if (c.getInt(0) != 0)
					return true;
				else 
					return false;
			}while(c.moveToNext());
		}else{
			return false;
		}
	}

	public long CUFBNews(News fb){
		if (CheckIfExist("tbl_fb_entries", "id", fb.id))
			return updateFBNews(fb);
		else
			return addFBNews(fb);
	}

	private long addFBNews(News fb){
		ContentValues cv = new ContentValues();

		cv.put("id", fb.id);
		cv.put("title", fb.title);
		cv.put("alternate_link", fb.alternate);
		cv.put("category_id", 0);
		cv.put("published_date", fb.published);
		cv.put("updated_date", fb.updated);
		cv.put("author_id", 0);
		cv.put("verb", "");
		cv.put("target", "");
		cv.put("objects", "");
		cv.put("comments", "");
		cv.put("likes", "");
		cv.put("content", fb.content);
		cv.put("is_read", 0);
		cv.put("html_content", fb.content);
		cv.put("thumb_link", fb.thumblink);

		//2012-07-05T20:43:41-07:00
		DateFormat formatter ; 
		Date date = null ; 
		formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		try {
			date = (Date)formatter.parse(fb.updated.replace("T", " "));
			System.out.println("Today is " +date.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		cv.put("timestamp", date.getTime());

		return db.insert("tbl_fb_entries", null, cv);
	}

	private long updateFBNews(News fb){
		ContentValues cv = new ContentValues();

		cv.put("title", fb.title);
		cv.put("alternate_link", fb.alternate);
		cv.put("category_id", 0);
		cv.put("published_date", fb.published);
		cv.put("updated_date", fb.updated);
		cv.put("author_id", 0);
		cv.put("verb", "");
		cv.put("target", "");
		cv.put("objects", "");
		cv.put("comments", "");
		cv.put("likes", "");
		cv.put("content", fb.content);
		cv.put("html_content", fb.content);
		cv.put("thumb_link", fb.thumblink);

		DateFormat formatter ; 
		Date date = null ; 
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = (Date)formatter.parse(fb.updated);
			System.out.println("Today is " +date.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		cv.put("timestamp", date.getTime());

		String strFilter = "id= '" + fb.id + "'";
		return db.update("tbl_fb_entries", cv, strFilter,null);
	}

	public long CUFBImage(Image img){
		String query = "SELECT * FROM tbl_image_link WHERE fb_entry_id = '" + img.fbid + "' AND image_link = '" + img.image_link + "'";
		catchLog("CheckImg Query : " + query);
		Cursor c = this.db.rawQuery(query, null);
		if (c.getCount() > 0)
			return updateFBImages(img);
		else
			return addFBImages(img);
	}

	private long addFBImages(Image img){
		ContentValues cv = new ContentValues();

		cv.put("fb_entry_id", img.fbid);
		cv.put("image_link", img.image_link);

		return db.insert("tbl_image_link", null, cv);
	}

	private long updateFBImages(Image img){
		ContentValues cv = new ContentValues();

		cv.put("fb_entry_id", img.fbid);
		cv.put("image_link", img.image_link);

		String strFilter = "fb_entry_id = '" + img.fbid + "' AND image_link = '" + img.image_link + "'";
		return db.update("tbl_image_link", cv, strFilter,null);
	}

	public long readNews(String nid){
		ContentValues cv = new ContentValues();
		cv.put("is_read", 1);
		String strFilter = "id= '" + nid + "'";
		catchLog("News " + nid + " has been read");
		return db.update("tbl_fb_entries", cv, strFilter,null);
	}

	public ArrayList<News> getAllFBNews(){
		ArrayList<News> fb = new ArrayList<News>();
		Date d = new Date();
		long first = d.getTime();
		d.setDate(d.getDate() - (storagetime * 7));
		long second = d.getTime();
		System.out.println("Storage Time => " + storagetime);
		String query = "SELECT * FROM tbl_fb_entries WHERE timestamp >= "+second+" AND timestamp < "+first+" ORDER BY timestamp DESC";
		//String query = "SELECT * FROM tbl_fb_entries ORDER BY idx DESC";
		catchLog("GetAllFBNews Query : " + query);
		Cursor c = this.db.rawQuery(query, null);
		if (c.moveToFirst()){
			do{
				News f = new News();

				f.id = c.getString(0);
				f.title = c.getString(1);
				f.alternate = c.getString(2);
				f.catId = c.getInt(3);
				f.published = c.getString(4);
				f.updated = c.getString(5);
				f.authorId = c.getInt(6);
				f.verb = c.getString(7);
				f.target = c.getString(8);
				f.object = c.getString(9);
				f.comment = c.getString(10);
				f.like = c.getString(11);
				f.content = c.getString(12);
				f.is_read = c.getInt(13);
				f.thumblink = c.getString(17);

				fb.add(f);

			}while (c.moveToNext());
		}
		return fb;
	}

	public ArrayList<Image> getAllFBImages(String fbid){
		ArrayList<Image> images = new ArrayList<Image>();
		String query = "SELECT * FROM tbl_image_link WHERE fb_entry_id = '" + fbid + "'";
		catchLog("GetAllFBImages Query : " + query);
		Cursor c = this.db.rawQuery(query, null);
		if (c.moveToFirst()){
			do{
				Image img = new Image();
				img.idx = c.getInt(0);
				img.fbid = c.getString(1);
				img.image_link = c.getString(2);

				images.add(img);

			}while (c.moveToNext());
		}
		return images;
	}

	public void autoDelete(){
		Date d = new Date();
		d.setDate(d.getDate() - 30);
		long last = d.getTime();
		System.out.println("Final Time => " + d);
		System.out.println("Final Time => " + last);
		String query = "DELETE FROM tbl_fb_entries WHERE timestamp <= " + last;
		db.execSQL(query);

	}

	public void CUAdv(Adv a){
		if (CheckIfExist("tbl_advertisement", "id", a.adid))
			updateAdv(a);
		else
			addAdv(a);
	}

	private long addAdv(Adv a){
		ContentValues cv = new ContentValues();
		cv.put("id", a.adid);
		cv.put("photo", a.photo);
		cv.put("url", a.url);
		cv.put("priority", a.priority);

		return db.insert("tbl_advertisement", null, cv);
	}

	private long updateAdv(Adv a){
		ContentValues cv = new ContentValues();
		cv.put("photo", a.photo);
		cv.put("url", a.url);
		cv.put("priority", a.priority);

		//return db.insert(TBL_NEWS, null, cv);
		String strFilter = "id=" + a.adid;
		return db.update("tbl_advertisement", cv, strFilter,null);
	}

	public Adv getAdv(int priority){
		Adv ad = new Adv();
		//SELECT * FROM table ORDER BY RANDOM() LIMIT 1;
		String query = "SELECT * FROM tbl_advertisement WHERE priority = " + priority + " ORDER BY RANDOM() LIMIT 1";
		catchLog("GetSpecificAdv Query : " + query);
		Cursor c = this.db.rawQuery(query, null);
		if (c.moveToFirst()){
			do{

				ad.adid = c.getString(0);
				ad.photo = c.getString(1);
				ad.url = c.getString(2);

			}while (c.moveToNext());
		}
		return ad;
	}

	public void clear(String tablename){
		String clear = "DELETE FROM " + tablename;
		catchLog("Clear Query => " + clear);
		db.execSQL(clear);
	}

	public synchronized void close(){
		if(db != null){
			db.close();
		}
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	private void catchLog(String log){
		Log.i(getClass().getName(), log);
	}
}

