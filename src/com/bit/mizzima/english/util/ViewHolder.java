package com.bit.mizzima.english.util;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ViewHolder{
	public TextView title;
	public ImageView photo;
	public TextView detail;
	public TouchImageView touchphoto;
	public ImageView facebook;
	public ImageView email;
	public ImageView adv;
}