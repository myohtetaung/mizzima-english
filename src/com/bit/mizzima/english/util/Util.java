package com.bit.mizzima.english.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpHost;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Proxy;
import android.util.Log;

public class Util {

	public static Typeface mm(Context cmx){
		return Typeface.createFromAsset(cmx.getAssets(),"fonts/Zawgyi-One-20060620.ttf");
	}
	
	public static void memoryusage(Activity a){
		MemoryInfo mi = new MemoryInfo();
		ActivityManager activityManager = (ActivityManager) a.getSystemService(a.ACTIVITY_SERVICE);
		activityManager.getMemoryInfo(mi);
		long availableMegs = mi.availMem / 1048576L;
		Log.v("MEM", "Avaiable Memory : " + availableMegs);
	}
	
	public static JSONObject central(Context cmx){
		JSONObject js = new JSONObject();

		try {
			String appid = "BITMZM0011";
			PackageManager pm = cmx.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(cmx.getPackageName(), 0);
			int versioncode = pi.versionCode;
			String udid = android.provider.Settings.System.getString(cmx.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
			js.put("app_id", appid);
			js.put("udid", udid);
			js.put("dp", "ytp");
			js.put("version", versioncode);
			js.put("platform", 2);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return js;
	}
	
	static void encrypt() throws Exception {
	    // Here you read the cleartext.
	    FileInputStream fis = new FileInputStream("data/cleartext");
	    // This stream write the encrypted text. This stream will be wrapped by another stream.
	    FileOutputStream fos = new FileOutputStream("data/encrypted");

	    // Length is 16 byte
	    SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
	    // Create cipher
	    Cipher cipher = Cipher.getInstance("AES");
	    cipher.init(Cipher.ENCRYPT_MODE, sks);
	    // Wrap the output stream
	    CipherOutputStream cos = new CipherOutputStream(fos, cipher);
	    // Write bytes
	    int b;
	    byte[] d = new byte[8];
	    while((b = fis.read(d)) != -1) {
	        cos.write(d, 0, b);
	    }
	    // Flush and close streams.
	    cos.flush();
	    cos.close();
	    fis.close();
	}
}
