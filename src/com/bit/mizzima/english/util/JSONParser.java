package com.bit.mizzima.english.util;

import org.json.JSONObject;

public class JSONParser {

	public int getInt(JSONObject json,String name){
		int value = 0;
		try {
			if (json.has(name))
				value = json.getInt(name);
			else
				value = 0;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	}
	
	public String getString(JSONObject json,String name){
		String value = "";
		try {
			if (json.has(name))
				value = json.getString(name);
			else
				value = "";
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	}
	
	public float getFloat(JSONObject json,String name){
		float value = 0;
		try {
			if (json.has(name))
				value = Float.parseFloat(json.getString(name));
			else
				value = 0;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	}
	
	public double getDouble(JSONObject json,String name){
		double value = 0;
		try {
			if (json.has(name))
				value = json.getDouble(name);
			else
				value = 0;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	}
}
