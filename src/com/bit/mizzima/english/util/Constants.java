package com.bit.mizzima.english.util;

public interface Constants {

	public static final int SPLASH_WAIT_MILLISEC = 1000; //  sec

	public static final String URL_FEED = "http://www.mizzima.com/fb_proxy_en.php";
	public static final String URL_AD = "http://www.bitmyanmar.com.mm/cms/mizzima/adsfeed.json";
	public static final String URL_CONFIG = "http://www.bitmyanmar.com.mm/cms/central2.php";
	public static final String URL_MSG = "http://www.bitmyanmar.com.mm/cms/mizzima/message_xml.php";
	public static final String URL_TRACKER = "http://www.bitmyanmar.com.mm/cms/mizzima/ads_tracker.php";

	public static final String SHARED_PRED = "MIZZIMA";

	public static final float fontsize_body = (float) 19.5;
	public static final float fontsize_title = 20;
	public static final float fontsize_hometitle = 8;
	public static final float linespacing = 1.2f;
	
	public static final int smallest = 16;
	public static final int small = 20;
	public static final int medium = 24;
	public static final int large = 28;

	public static final String BASEDIR = android.os.Environment.getExternalStorageDirectory() +"/data/mizzima";

	public static final String critism_code = "5036127a6c36f90282000002";
}
