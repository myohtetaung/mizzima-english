package com.bit.mizzima.english;

import java.io.IOException;
import java.util.ArrayList;

import androd.httpimage.FileSystemPersistence;
import androd.httpimage.HttpImageManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.bit.mizzima.english.adapter.DetailPagerAdapter;
import com.bit.mizzima.english.object.News;
import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.util.DBManager;
import com.bit.mizzima.english.R;

public class DetailPager extends SherlockActivity implements OnPageChangeListener {

	DBManager dbm;
	HttpImageManager mManager;
	ArrayList<News> news = new ArrayList<News>();
	SharedPreferences pref;
	int position;

	ViewPager vp;
	DetailPagerAdapter dpa;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		init_Manager();
		init_data();
		init_UI();
	}

	private void init_Manager(){
		dbm = new DBManager(getApplicationContext());
		
		pref = getSharedPreferences(Constants.SHARED_PRED, Context.MODE_PRIVATE);
		mManager = new HttpImageManager(new FileSystemPersistence(Constants.BASEDIR));
	}

	private void init_data(){
		Bundle extras = getIntent().getExtras();
		if (extras!=null){
			position = getIntent().getIntExtra("NPOS", 0);
		}else{
			position = pref.getInt("NPOS", 0);
		}

		news = dbm.getAllFBNews();
	}

	private void init_UI(){
		vp = (ViewPager) findViewById(R.id.news_pager);
		dpa = new DetailPagerAdapter(this,dbm, mManager, news);
		vp.setAdapter(dpa);
		vp.setCurrentItem(position);
		vp.setOnPageChangeListener(this);
		dbm.readNews(news.get(position).id);

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub
		dbm.readNews(news.get(arg0).id);
	}
}
