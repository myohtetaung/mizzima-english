package com.bit.mizzima.english;

import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		Thread splashTread = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(Constants.SPLASH_WAIT_MILLISEC);
				} catch (Exception e) {
					// do nothing
					e.printStackTrace();
				} finally {
					startActivity(new Intent(getApplicationContext(),
							MainActivity.class));
					finish();
				}
			}
		};

		splashTread.start();
	}
}
