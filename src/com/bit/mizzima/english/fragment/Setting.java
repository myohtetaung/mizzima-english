package com.bit.mizzima.english.fragment;


import com.actionbarsherlock.app.SherlockFragment;
import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.R;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Setting extends SherlockFragment implements OnClickListener {

	TextView font_smallest,font_small,font_medium,font_large;
	TextView storage_smallest,storage_small,storage_medium,storage_large;
	SharedPreferences pref;
	Editor edit;

	public Setting(Context c){
		pref = c.getSharedPreferences(Constants.SHARED_PRED, Context.MODE_PRIVATE);
		edit = pref.edit();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fragment_setting, null);

		font_smallest = (TextView) v.findViewById(R.id.font_smallest);
		font_small = (TextView) v.findViewById(R.id.font_small);
		font_medium = (TextView) v.findViewById(R.id.font_medium);
		font_large = (TextView) v.findViewById(R.id.font_large);

		storage_smallest = (TextView) v.findViewById(R.id.storage_smallest);
		storage_small = (TextView) v.findViewById(R.id.storage_small);
		storage_medium = (TextView) v.findViewById(R.id.storage_medium);
		storage_large = (TextView) v.findViewById(R.id.storage_large);

		init_SET();
		init_Storage();

		font_smallest.setOnClickListener(this);
		font_small.setOnClickListener(this);
		font_medium.setOnClickListener(this);
		font_large.setOnClickListener(this);

		storage_smallest.setOnClickListener(this);
		storage_small.setOnClickListener(this);
		storage_medium.setOnClickListener(this);
		storage_large.setOnClickListener(this);

		return v;


	}

	private void init_SET(){
		switch (pref.getInt("FONTSIZE", 16)) {
		case 16:
			font_smallest.setTextColor(Color.RED);
			font_smallest.setTypeface(null, Typeface.BOLD);
			font_small.setTextColor(Color.DKGRAY);
			font_medium.setTextColor(Color.DKGRAY);
			font_large.setTextColor(Color.DKGRAY);
			break;
		case 20:
			font_smallest.setTextColor(Color.DKGRAY);
			font_small.setTextColor(Color.RED);
			font_small.setTypeface(null, Typeface.BOLD);
			font_medium.setTextColor(Color.DKGRAY);
			font_large.setTextColor(Color.DKGRAY);
			break;
		case 24:
			font_smallest.setTextColor(Color.DKGRAY);
			font_small.setTextColor(Color.DKGRAY);
			font_medium.setTextColor(Color.RED);
			font_medium.setTypeface(null, Typeface.BOLD);
			font_large.setTextColor(Color.DKGRAY);
			break;
		case 28:
			font_smallest.setTextColor(Color.DKGRAY);
			font_small.setTextColor(Color.DKGRAY);
			font_medium.setTextColor(Color.DKGRAY);
			font_large.setTextColor(Color.RED);
			font_large.setTypeface(null, Typeface.BOLD);
			break;
		default:
			font_smallest.setTextColor(Color.DKGRAY);
			font_small.setTextColor(Color.DKGRAY);
			font_medium.setTextColor(Color.DKGRAY);
			font_large.setTextColor(Color.RED);
			font_large.setTypeface(null, Typeface.BOLD);
			break;
		}
	}

	private void init_Storage(){
		switch (pref.getInt("STORAGE", 1)) {
		case 1:
			storage_smallest.setTextColor(Color.RED);
			storage_smallest.setTypeface(null, Typeface.BOLD);
			storage_small.setTextColor(Color.DKGRAY);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_large.setTextColor(Color.DKGRAY);
			break;
		case 2:
			storage_smallest.setTextColor(Color.DKGRAY);
			storage_small.setTextColor(Color.RED);
			storage_small.setTypeface(null, Typeface.BOLD);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_large.setTextColor(Color.DKGRAY);
			break;
		case 3:
			storage_smallest.setTextColor(Color.DKGRAY);
			storage_small.setTextColor(Color.DKGRAY);
			storage_medium.setTextColor(Color.RED);
			storage_medium.setTypeface(null, Typeface.BOLD);
			storage_large.setTextColor(Color.DKGRAY);
			break;
		case 4:
			storage_smallest.setTextColor(Color.DKGRAY);
			storage_small.setTextColor(Color.DKGRAY);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_large.setTextColor(Color.RED);
			storage_large.setTypeface(null, Typeface.BOLD);
			break;
		default:
			storage_smallest.setTextColor(Color.DKGRAY);
			storage_small.setTextColor(Color.DKGRAY);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_large.setTextColor(Color.RED);
			storage_large.setTypeface(null, Typeface.BOLD);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.font_smallest:

			font_smallest.setTextColor(Color.RED);
			font_smallest.setTypeface(null, Typeface.BOLD);
			font_small.setTextColor(Color.DKGRAY);
			font_small.setTypeface(null, Typeface.NORMAL);
			font_medium.setTextColor(Color.DKGRAY);
			font_medium.setTypeface(null, Typeface.NORMAL);
			font_large.setTextColor(Color.DKGRAY);
			font_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("FONTSIZE", 16);
			break;
		case R.id.font_small:

			font_smallest.setTextColor(Color.DKGRAY);
			font_smallest.setTypeface(null, Typeface.NORMAL);
			font_small.setTextColor(Color.RED);
			font_small.setTypeface(null, Typeface.BOLD);
			font_medium.setTextColor(Color.DKGRAY);
			font_medium.setTypeface(null, Typeface.NORMAL);
			font_large.setTextColor(Color.DKGRAY);
			font_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("FONTSIZE", 20);
			break;
		case R.id.font_medium:

			font_smallest.setTextColor(Color.DKGRAY);
			font_smallest.setTypeface(null, Typeface.NORMAL);
			font_small.setTextColor(Color.DKGRAY);
			font_small.setTypeface(null, Typeface.NORMAL);
			font_medium.setTextColor(Color.RED);
			font_medium.setTypeface(null, Typeface.BOLD);
			font_large.setTextColor(Color.DKGRAY);
			font_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("FONTSIZE", 24);
			break;
		case R.id.font_large:

			font_smallest.setTextColor(Color.DKGRAY);
			font_smallest.setTypeface(null, Typeface.NORMAL);
			font_small.setTextColor(Color.DKGRAY);
			font_small.setTypeface(null, Typeface.NORMAL);
			font_medium.setTextColor(Color.DKGRAY);
			font_medium.setTypeface(null, Typeface.NORMAL);
			font_large.setTextColor(Color.RED);
			font_large.setTypeface(null, Typeface.BOLD);

			edit.putInt("FONTSIZE", 28);
			break;
		case R.id.storage_smallest:

			storage_smallest.setTextColor(Color.RED);
			storage_smallest.setTypeface(null, Typeface.BOLD);
			storage_small.setTextColor(Color.DKGRAY);
			storage_small.setTypeface(null, Typeface.NORMAL);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_medium.setTypeface(null, Typeface.NORMAL);
			storage_large.setTextColor(Color.DKGRAY);
			storage_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("STORAGE", 1);
			break;
		case R.id.storage_small:

			storage_smallest.setTextColor(Color.DKGRAY);
			storage_smallest.setTypeface(null, Typeface.NORMAL);
			storage_small.setTextColor(Color.RED);
			storage_small.setTypeface(null, Typeface.BOLD);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_medium.setTypeface(null, Typeface.NORMAL);
			storage_large.setTextColor(Color.DKGRAY);
			storage_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("STORAGE", 2);
			break;
		case R.id.storage_medium:

			storage_smallest.setTextColor(Color.DKGRAY);
			storage_smallest.setTypeface(null, Typeface.NORMAL);
			storage_small.setTextColor(Color.DKGRAY);
			storage_small.setTypeface(null, Typeface.NORMAL);
			storage_medium.setTextColor(Color.RED);
			storage_medium.setTypeface(null, Typeface.BOLD);
			storage_large.setTextColor(Color.DKGRAY);
			storage_large.setTypeface(null, Typeface.NORMAL);

			edit.putInt("STORAGE", 3);
			break;
		case R.id.storage_large:

			storage_smallest.setTextColor(Color.DKGRAY);
			storage_smallest.setTypeface(null, Typeface.NORMAL);
			storage_small.setTextColor(Color.DKGRAY);
			storage_small.setTypeface(null, Typeface.NORMAL);
			storage_medium.setTextColor(Color.DKGRAY);
			storage_medium.setTypeface(null, Typeface.NORMAL);
			storage_large.setTextColor(Color.RED);
			storage_large.setTypeface(null, Typeface.BOLD);

			edit.putInt("STORAGE", 4);
			break;
		default:
			break;
		}
		edit.commit();
	}
}
