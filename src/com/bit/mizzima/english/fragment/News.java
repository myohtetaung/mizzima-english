package com.bit.mizzima.english.fragment;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.bit.mizzima.english.adapter.NewsAdapter;
import com.bit.mizzima.english.async.GetNews;
import com.bit.mizzima.english.util.DBManager;
import com.bit.mizzima.english.util.PullToRefreshListView;
import com.bit.mizzima.english.util.PullToRefreshListView.OnRefreshListener;
import com.bit.mizzima.english.R;

public class News extends SherlockFragment {

	DBManager dbm;

	PullToRefreshListView lv;
	ImageView ad;

	public News() {
		dbm = new DBManager(getActivity());
		// init_DBM();
	}

	/*
	 * private void init_DBM(){ try { dbm.createDataBase(); } catch (IOException
	 * e) { throw new Error("Unable to create database"); }
	 * 
	 * try { dbm.openDatabase(); } catch (SQLiteException sqle) { throw sqle; }
	 * 
	 * }
	 */

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_news, null);

		lv = (PullToRefreshListView) v.findViewById(R.id.news_list);

		lv.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				GetNews news = new GetNews(getActivity(), dbm, lv);
				news.execute();
			}
		});

		ArrayList<com.bit.mizzima.english.object.News> n = dbm.getAllFBNews();
		lv.setAdapter(new NewsAdapter(getActivity(), R.layout.newslist_item, n));
		GetNews news = new GetNews(getActivity(), dbm, lv);
		news.execute();

		return v;
	}

	private void catchLog(String log) {
		Log.i(getClass().getName(), log);
	}
}
