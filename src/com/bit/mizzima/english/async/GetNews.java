package com.bit.mizzima.english.async;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bit.mizzima.english.adapter.NewsAdapter;
import com.bit.mizzima.english.object.Adv;
import com.bit.mizzima.english.object.Image;
import com.bit.mizzima.english.object.News;
import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.util.DBManager;
import com.bit.mizzima.english.util.HTTPPost;
import com.bit.mizzima.english.util.JSONParser;
import com.bit.mizzima.english.util.NetworkListener;
import com.bit.mizzima.english.util.PullToRefreshListView;
import com.bit.mizzima.english.util.Util;
import com.bit.mizzima.english.util.ViewHolder;
import com.bit.mizzima.english.util.XMLParser;
import com.bit.mizzima.english.R;

import androd.httpimage.FileSystemPersistence;
import androd.httpimage.HttpImageManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class GetNews extends AsyncTask<Void, Void, Void> {

	private Context _c;
	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;

	private boolean _show;

	private ProgressDialog progressDialog;
	private DBManager dbm;
	private HTTPPost post;

	HttpImageManager iManager;

	private ArrayList<News> news = new ArrayList<News>();
	private PullToRefreshListView listview;
	JSONParser parse;

	static boolean setfooter = false;

	public GetNews(Context c, DBManager dbm, PullToRefreshListView listview) {
		_c = c;
		this.dbm = dbm;
		parse = new JSONParser();
		preferences = _c.getSharedPreferences(Constants.SHARED_PRED, 0);
		editor = preferences.edit();
		this._show = preferences.getBoolean("FIRST", true);

		progressDialog = new ProgressDialog(_c);
		progressDialog.setTitle("");
		progressDialog.setMessage("Loading.Please wait.....");

		iManager = new HttpImageManager(new FileSystemPersistence(
				Constants.BASEDIR));

		this.listview = listview;
	}

	@Override
	protected void onPreExecute() {
		if (_show) {
			progressDialog.show();
		}
		this.listview.setRefreshing();
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {

			if (NetworkListener.isOnline(_c)) {
				catchLog("Connected");
				catchLog(_c.getPackageName());

				post = new HTTPPost();

				String res = post.sendJSON(Util.central(_c), preferences
						.getString("config_link", Constants.URL_CONFIG));
				catchLog("HTTP Result : " + res);
				parseXML(res);

				getNews();
				getAdvs();
				// End Getting News and setting

			} else {
				catchLog("NOT Connected");
				catchLog("Get both from local db");
			}

		} catch (Exception e) {
			e.printStackTrace();
			catchLog("Connection Error");
			catchLog("Get both from local db");
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if (progressDialog.isShowing())
			progressDialog.dismiss();
		editor.putBoolean("FIRST", false);
		editor.commit();
		news = dbm.getAllFBNews();
		catchLog("Total News => " + news.size());
		this.listview.onRefreshComplete();

		final Adv adv = dbm.getAdv(1);
		catchLog(adv.photo);
		ViewHolder vh = new ViewHolder();
		vh.adv = new ImageView(_c);
		vh.adv.setPadding(20, 10, 20, 10);
		String udid = android.provider.Settings.System.getString(
				_c.getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		final String link = preferences.getString("tracker_link",
				Constants.URL_TRACKER)
				+ "?adid="
				+ adv.adid
				+ "&url="
				+ adv.url
				+ "&app_id=BITMZM0011"
				+ "&udid="
				+ udid
				+ "&platform=2";

		if (!adv.photo.trim().equalsIgnoreCase("") && adv.photo != null) {
			// vh.photo.setTag(adv.photo);
			vh.adv.setTag(adv.photo);
			Uri uri = Uri.parse(adv.photo);
			if (uri != null) {
				Bitmap bitmap = iManager
						.loadImage(new HttpImageManager.LoadRequest(uri, vh.adv));

				if (bitmap != null) {
					vh.adv.setImageBitmap(bitmap);
				}
			}
		}
		/*
		 * if(listview.getFooterViewsCount()>0){
		 * catchLog("Footerview remove - "+listview.removeFooterView(img_adv));
		 * }
		 */
		this.listview.setAdapter(new NewsAdapter(_c, R.layout.newslist_item,
				news));
		if (!setfooter) {
			setfooter = true;
			listview.addFooterView(vh.adv);
			vh.adv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent a = new Intent(Intent.ACTION_VIEW);
					catchLog(link);
					a.setData(Uri.parse(link));
					_c.startActivity(a);
				}
			});
		}
	}

	private void getNews() {
		try {
			catchLog(preferences.getString("news_link", Constants.URL_FEED));
			String result = post.requestFeed(preferences.getString(
					"movie_link", Constants.URL_FEED));

			catchLog("HTTP News result : " + result);

			JSONObject user = new JSONObject(post.toJSONString(result));
			catchLog("Complete JSON Object ==> " + user.toString());

			JSONArray ent = user.getJSONArray("entries");
			catchLog("Complete Entity JSON Array == >" + ent.toString());

			catchLog("JSON Length ==> " + ent.length());

			dbm.autoDelete();

			for (int j = 0; j < ent.length(); j++) {
				JSONObject entity = ent.getJSONObject(j);
				catchLog("Entity JSON Object ==> " + entity.toString());

				News n = new News();

				n.title = parse.getString(entity, "title");
				n.id = parse.getString(entity, "id");
				n.alternate = parse.getString(entity, "alternate");
				n.published = parse.getString(entity, "published");
				n.updated = parse.getString(entity, "updated");

				// For Note Link
				n.content = getContent(entity).replaceAll("<img.+?>", "")
						.replaceAll("<a.+?>", "").replaceAll("</a>", "");
				if (!getImage(entity).equalsIgnoreCase(
						"http://www.facebook.com/"))
					n.thumblink = getImage(entity);

				long idx = dbm.CUFBNews(n);

				getImage(getContent(entity), idx, n.id);

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private String getImage(JSONObject js) {
		String result = "";
		try {
			JSONObject image = js.getJSONObject("image");
			result = parse.getString(image, "url");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	private String getContent(JSONObject js) {
		String result = "";
		try {
			JSONObject content = js.getJSONObject("content");
			result = parse.getString(content, "__html");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	private void getAdvs() {
		try {
			catchLog(preferences.getString("ads_link", Constants.URL_AD));
			String result = post.requestFeed(preferences.getString("ads_link",
					Constants.URL_AD));

			catchLog("HTTP News result : " + result);

			JSONArray ent = new JSONArray(result);
			catchLog("Complete Entity JSON Array == >" + ent.toString());

			catchLog("JSON Lenght ==> " + ent.length());
			dbm.clear("tbl_advertisement");
			for (int i = 0; i < ent.length(); i++) {
				JSONObject obj = ent.getJSONObject(i);
				catchLog("Entity JSON Object ==> " + obj.toString());

				JSONParser parse = new JSONParser();

				Adv adv = new Adv();

				adv.adid = parse.getString(obj, "adid");
				adv.photo = parse.getString(obj, "photo");
				adv.priority = parse.getInt(obj, "priority");
				adv.url = parse.getString(obj, "url");

				dbm.CUAdv(adv);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void getImage(String content, long idx, String fbid) {
		try {
			Matcher match = Pattern.compile(
					"<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>").matcher(
					content);

			Image img = new Image();
			img.idx = idx;
			img.fbid = fbid;

			while (match.find()) {
				catchLog("Image Link ==> " + match.group(1));
				img.image_link = match.group(1);
				dbm.CUFBImage(img);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void parseXML(String xml) {
		XMLParser parse = new XMLParser();
		Document doc = parse.getDomElement(xml);
		NodeList nl = doc.getElementsByTagName("config");

		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);

			editor.putString("news_link", parse.getValue(e, "news_link"));
			editor.putString("ads_link", parse.getValue(e, "ads_link"));
			editor.putString("config_link", parse.getValue(e, "config_link"));
			editor.putString("message_link", parse.getValue(e, "message_link"));
			editor.putString("tracker_link", parse.getValue(e, "tracker_link"));
		}

		editor.commit();
	}

	private void catchLog(String log) {
		Log.i("GetMovies", log);
	}
}
