package com.bit.mizzima.english.messaging;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Push_Notification extends BroadcastReceiver{

	private Context context;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		this.context = context;
		startService();
	}
	
	public void startService(){
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE );
		NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE|ConnectivityManager.TYPE_WIFI);
		boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
		
		Calendar cur_cal = Calendar.getInstance();
		cur_cal.setTimeInMillis(System.currentTimeMillis());
		Intent i = new Intent(context, Push_Notification_Checker.class);
		PendingIntent pintent = PendingIntent.getService(context, 0, i, 0);
		AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (isConnected){
			Log.i("NET", "connecte " +isConnected);
			alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 1 * 60*1000, pintent);
		}
		else {
			Log.i("NET", "not connecte " +isConnected);
			alarm.cancel(pintent);
		}
	}

}
