package com.bit.mizzima.english.messaging;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bit.mizzima.english.util.Constants;
import com.bit.mizzima.english.util.HTTPPost;
import com.bit.mizzima.english.util.Util;
import com.bit.mizzima.english.util.XMLParser;
import com.bit.mizzima.english.R;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class Push_Notification_Checker extends Service {

	NotificationManager nm;
	SharedPreferences pref;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.d("NET", "Service got created");
		nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		pref = getSharedPreferences(Constants.SHARED_PRED, 0);
		new getData().execute();
		//Toast.makeText(this, "ServiceClass.onCreate()", Toast.LENGTH_LONG).show();
	}
	
	private class getData extends android.os.AsyncTask<Void, Void, Void>{

		String res;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HTTPPost post = new HTTPPost();
			//String result = post.getHttpData(url);
			res = post.sendJSON(Util.central(getApplicationContext()), pref.getString("message_link", Constants.URL_MSG));
			Log.i("NET","HTTP Result : "+res);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if (res != null && res != "") parseXML(res);
		}
		
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.d("NET", "Service got destroyed");
		//Toast.makeText(this, "ServiceClass.onDestroy()", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		//Toast.makeText(this, "ServiceClass.onStart()", Toast.LENGTH_LONG).show();
		Log.d("NET", "Service got started");
	}

	private void parseXML(String xml){
		XMLParser parse = new XMLParser();
		Document doc = parse.getDomElement(xml);
		NodeList nl = doc.getElementsByTagName("messages");

		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);

			Log.i("NET","Message Decription : "+ parse.getValue(e, "message_decr"));
			Log.i("NET","Message Type : "+ parse.getValue(e, "message_type"));
			Log.i("NET","Message Action Type : "+ parse.getValue(e, "action_type"));
			Log.i("NET","Message Language : "+ parse.getValue(e, "language"));
			Log.i("NET","Message URL : "+ parse.getValue(e, "url"));

			if (!parse.getValue(e, "message_decr").equalsIgnoreCase(""))
				showNotification(parse.getValue(e, "message_decr"), parse.getValue(e, "url"));
		}
	}

	private void showNotification(String content,String link) {
		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_launcher, content, System.currentTimeMillis());
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
		notificationIntent.setData(Uri.parse(link));

		// The PendingIntent to launch our activity if the user selects this notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, "Mizzima", content, contentIntent);

		// Send the notification.
		// We use a layout id because it is a unique number.  We use it later to cancel.
		nm.notify(10, notification);
	}

}
