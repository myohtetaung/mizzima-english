package com.bit.mizzima.english;

import java.io.IOException;
import java.util.Calendar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.bit.mizzima.english.adapter.PagerAdapter;
import com.bit.mizzima.english.messaging.Push_Notification_Checker;
import com.bit.mizzima.english.util.DBManager;
import com.bit.mizzima.english.R;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends SherlockFragmentActivity implements TabListener {

	PagerAdapter adapter;
	ViewPager pager;
	DBManager dbm;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dbm = new DBManager(getApplicationContext());
		init();
		init_UI();
	}
	
	private void init(){
		adapter = new PagerAdapter(getApplicationContext(),getSupportFragmentManager());
		startService();
	}
	
	public void startService(){
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE );
		NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE|ConnectivityManager.TYPE_WIFI);
		boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();

		Calendar cur_cal = Calendar.getInstance();
		cur_cal.setTimeInMillis(System.currentTimeMillis());
		Intent i = new Intent(getApplicationContext(), Push_Notification_Checker.class);
		PendingIntent pintent = PendingIntent.getService(getApplicationContext(), 0, i, 0);
		AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		if (isConnected){
			Log.i("NET", "connecte " +isConnected);
			alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 60*1000, pintent);
		}
		else {
			Log.i("NET", "not connecte " +isConnected);
			alarm.cancel(pintent);
		}
	}

	private void init_UI(){
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });
		
		for(int i=0;i<adapter.getCount();i++){
			 actionBar.addTab(
	                    actionBar.newTab()
	                            .setText(adapter.getPageTitle(i))
	                            .setTabListener(this));
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		//first saving my state, so the bundle wont be empty.
		//http://code.google.com/p/android/issues/detail?id=19917
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		pager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
